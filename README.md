# 黑苹果基于 opencore UEFI 引导文件

## 我的电脑配置

| 硬件类型 | 硬件型号                                                  |
| -------- | --------------------------------------------------------- |
| 处理器   | AMD Ryzen 5 3600 6-Core 六核                              |
| 主板     | 微星 B450M MORTAR MAX(MS-7B89)                            |
| 内存     | 16GB 海盗船 DDR4 3200MHz                                  |
| 主硬盘   | SATA 英特尔 ssd 240GB                                     |
| 显卡     | AMD Radeon RX 5600XT (蓝宝石)                             |
| 声卡     | 瑞昱 ALC892 @ AMD High Definition Audio 控制出去          |
| 网卡     | 瑞昱 RTL8168/8111/8112 Gigabit Ethernet Controller / 微星 |
